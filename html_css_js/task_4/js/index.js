'use strict'
console.log("task 1.\n");
var a = "1", b = "1";
var strc = conc(a,b);
console.log("first param: " + a + "\nsecond param: " + b);
console.log("result: " + strc);
function conc(a,b) {
    return a+b;
}

console.log("\ntask 2.\n");
a = "1"; b = "1";
//var res = comp(a,b);//я делал по заданию и естественно будет ошибка, но я как понимываю это и требовалось
console.log("first param: " + a + "\nsecond param: " + b);
//console.log("result: " + res);
a = "1"; b = "-1";
//res = comp(a,b);
console.log("first param: " + a + "\nsecond param: " + b);//             wat
//console.log("result: " + res);
var comp = function (a,b) {
    if(a === b) return 1;
    else return -1;
}

console.log("\ntask 3.\n");
// in HTML

console.log("\ntask 4.\n");
var n = 7;
var fib = function fibo(n) {
    if(n == 1) return 1;
    if(n == 0) return 0;
    return fibo(n-1) + fibo(n-2);
}
console.log("Для числа: " + n + "\nЧисло Фибоначчи: " + fib(n));

console.log("\ntask 5.\n");
a = "1"; b = "1";
console.log("first param: " + a + "\nsecond param: " + b);
console.log("result: " +
(function conc(a,b) {
    return a+b;
})(a,b));

console.log("\ntask 6.\n");
var groupItems = function () {
    var itms = [], j = 0;
    for(var i in arguments){
        if(/:([^.]*)/i.test(arguments[i])) {
            itms[j] = arguments[i].match(/:([^.]*)/i)[1];
            j++;
        }
    }
    return itms;
}
var param1 = "This is the first sentence. This is a sentence with a list of items: cherries, oranges, apples, bananas.";
var param2 = "This is the second sentence. This is a sentence with a list of items: red, blue, yellow, black.";
console.log("first sentence: " + param1);
console.log("second sentence: " + param2);
console.log("result: " + groupItems(param1,param2));

console.log("\ntask 7.\n");
var find = function(testString, test){
    if(test === undefined) test = testString;
    return testString.indexOf(test);
}
var testString = 'abc', test ='b';
console.log("testString: " + testString);
console.log("test: " + test);
console.log("index: " + find(testString,test));

console.log("\ntask 8.\n");
function str(a) {
    if(a === undefined || a === "") alert("String is empty");
    else alert("String is non empty");
}
str.isNonEmptyStr = function (a) {
        if(a === undefined || a === "") return false;
        else return true;
    }
console.log("Проверка на пустой строке");
str("");
console.log(str.isNonEmptyStr(""));

console.log("\ntask 9.\n");
function toConsole(msg) {
    console.log(msg);
}
function toAlert(msg) {
    alert(msg);
}
function splitToWords(msg, callback) {
    var a = msg.split(' ');
    if(callback !== undefined){
        for(var i in a)
            callback(a[i]);
    }
    else return a;
}
splitToWords("My very long text msg", toConsole);
splitToWords("My very long text msg", toAlert);
console.log(splitToWords("My very long text msg"));

console.log("\ntask 10.\n");
function copyright() {
    var sign = "\u00A9 ";
    return function (str) {
        return sign + str;
    };
}
console.log(copyright()("EPAM"));

console.log("\ntask 11.\n");
var Employee = {
    name: "Ann",
    work: function () {
        console.log("I am "+ this.name +". I am working...");
    }
};
Employee.work();