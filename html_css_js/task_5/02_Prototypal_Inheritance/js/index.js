'use strict'
var domElement = Object.create({
    init: function (element) {
        this.__domElement__ = document.createElement(element);
        return this;
    },
    appendChild: function (child) {
        this.__domElement__.appendChild(child.__domElement__);
        return this;
    },
    addAttribute: function (name, value) {
        if(name != "" && value !== undefined) {
            this.__domElement__.setAttribute(name, value);
        }
        else throw Error("Error");
        return this;
    },
    removeAttribute: function (attr) {
        if(this.__domElement__.getAttribute(attr) != "" && this.__domElement__.getAttribute(attr) !== null){
            this.__domElement__.removeAttribute(attr);
        }
        else throw Error("Error");
        return this;
    }
});
Object.defineProperty(domElement,"content",{
    set: function(val){
       if(this.__domElement__.childNodes.length == 0){
           this.__domElement__.innerHTML = val;
        }
    }
});
Object.defineProperties(domElement,{
    "children":{
        get: function () {
            return this.__domElement__.childNodes;
        }
    },
    "parent":{
        get: function () {
            return this.__domElement__.parentNode;
        }
    },
    "attributes":{
        get:function () {
            return this.__domElement__.attributes;
        }
    },
    "innerHTML":{
        get: function () {
            var s = this.__domElement__.outerHTML;
            for(var j = 0; j < this.__domElement__.childNodes.length; j++) {
                if (this.__domElement__.childNodes[j].attributes.length > 1) {
                    var ar = [];
                    for (var i = 0; i < this.__domElement__.childNodes[j].attributes.length; i++) {
                        ar[i] = this.__domElement__.childNodes[j].attributes[i];
                    }
                    ar.sort(function (a, b) {
                        if (a.name > b.name) return 1;
                        if (a.name < b.name) return -1;
                        return 0;
                    });
                    var str = this.__domElement__.childNodes[j].outerHTML;
                    for (var i = 1; i < ar.length; i++) {
                         str = str.replace(this.__domElement__.childNodes[j].attributes[i].name, ar[i].name);
                        str = str.replace(this.__domElement__.childNodes[j].attributes[i].value, ar[i].value);
                    }
                    str = str.replace(this.__domElement__.childNodes[j].attributes[0].name, ar[0].name);
                    str = str.replace(this.__domElement__.childNodes[j].attributes[0].value, ar[0].value);
                    s = s.replace(this.__domElement__.childNodes[j].outerHTML, str);
                }
            }
            return s;//вы хотите, чтоб как innerHTML, но в примере как outerHTML.    WAT?
        }
    }
});
var meta = Object.create(domElement).init('meta').addAttribute('charset', 'utf-8');

var head = Object.create(domElement).init('head').appendChild(meta);

var div = Object.create(domElement).init('div').addAttribute('style', 'font-size: 42px');

div.content = 'Hello, world!';

var body = Object.create(domElement).init('body').appendChild(div).addAttribute('id', 'myid').addAttribute('bgcolor', '#012345');

var root = Object.create(domElement).init('html').appendChild(head).appendChild(body);

console.log(root.innerHTML);