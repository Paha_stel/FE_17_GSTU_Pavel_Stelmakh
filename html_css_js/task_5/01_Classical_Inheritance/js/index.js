'use strict'
function Person() {

    Object.defineProperties(this, {
        'firstName': {
            get: function () {
                return this._fName;
            },
            set: function (val) {
                if(val.length > 2 && val.length <= 20 && val.length == val.match(/\w*/)[0].length){
                    this._fName = val;
                }
                else throw new Error("Error");
            }
        },
        'lastName': {
            get: function () {
                return this._lName;
            },
            set: function (val) {
                if(val.length > 2 && val.length <= 20 && val.length == val.match(/\w*/)[0].length){
                    this._lName = val;
                }
                else throw new Error("Error");
            }
        },
        'age': {
            get: function () {
                return this._age;
            },
            set: function (val) {
                if(typeof val != "number") {
                    if (parseInt(val) >= 0 && parseInt(val) <= 150 && val.match(/\d*/)[0].length == val.length) {
                        this._age = parseInt(val);
                    }
                    else throw new Error("Error");
                }
                else this._age = val;
            }
        },
        'fullname': {
            get: function () {
                return this.firstName + " " + this.lastName;
            },
            set: function (val) {
                var a = val.split(' ');
                if(a.length == 2){
                    this.firstName = a[0];
                    this.lastName = a[1];
                }
                else throw new Error("Error");
            }
        }
    });

};

Person.prototype.Introduction = function() {
    console.log("Hello! My name is " + this.firstName + " and I am " + this.age + "-years-old");
};

var person = new Person();
console.log(person.fullname);
person.fullname = "Pavel Stelmakh";
person.age = 34;
person.Introduction();