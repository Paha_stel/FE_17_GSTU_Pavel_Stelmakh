'use strict'
console.log("task 1.\n");

function LastDigit(digit) {
    var arrayNumders = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"];
    console.log("the last digit of a number " + digit + " is " + arrayNumders[digit%10])
}

var a = Math.floor(Math.random()*1000);
LastDigit(a);

console.log("\ntask 2.\n");
function ReversDigit(digit) {
    digit = digit.toString();
    var arChar = [];
    for(var i = 0; i < digit.length; i++) arChar[i] = digit[i];
    digit = Number(arChar.reverse().join(''));
    return digit;
}
a = Math.floor(Math.random()*1000);
console.log("Number " + a);
console.log("Reverse number " + ReversDigit(a));

console.log("\ntask 3.\n");
var text = "TExt. Here are very many text. text TeXt. TEXT text text";
var word = "text";
console.log(text + '\n' + word + "\nall the occurrences of word in a text:\n");
(function findWord(text, word, withCase) {
    if(withCase === undefined) {
        var pos = 0;
        do{
            pos = text.indexOf(word, pos);
            if(pos != -1) console.log(text.slice(pos));
        }while(++pos);
    }
    else{
        var pos = 0;
        var tempText = text; tempText = tempText.toLowerCase();
        do{
            pos = tempText.indexOf(word, pos);
            if(pos != -1) console.log(text.slice(pos));
        }while(++pos);
    }
})(text,word, true);

console.log("\ntask 4.\n");
function CountDiv() {
    var divs = document.getElementsByTagName("div");
    console.log(divs);
    return divs.length;
}
console.log("Count tag \"div\": " + CountDiv());

console.log("\ntask 5.\n");
function CountDigitInArray(array, key) {
    for(var i = 0; i < array.length; i++){
        if(array[i] == key) return true;
    }
    return false;
}
(function TestCountDigitInArray() {
    for(var i = 0; i < 3; i++){
        var a = [];
        for(var j = 0; j < Math.floor(Math.random()*8 + 1); j++) a[j] = Math.floor(Math.random()*6);
        var k = Math.floor(Math.random()*7);
        console.log("array: " + a);
        console.log("key: " + k);
        console.log(CountDigitInArray(a,k));
    }
})();

console.log("\ntask 6.\n");
function ChecksInArray(array, index) {
    if(index >= 0 && array.length > index){
        if(array[index + 1] !== undefined && array[index - 1] !== undefined){
            if(array[index + 1] < array[index] && array[index - 1] < array[index]) return 1;
            else return 0;
        }
        else return -1;
    }
    else return undefined;
}
(function TestChecksInArray() {
    for(var i = 0; i < 3; i++){
        var a = [];
        for(var j = 0; j < Math.floor(Math.random()*8 + 1); j++) a[j] = Math.floor(Math.random()*7);
        var ind = Math.floor(Math.random()*7);
        console.log("array: " + a);
        console.log("index: " + ind);
        if(ChecksInArray(a,ind) === 1 ) console.log("Больше своих соседей");
        else if(ChecksInArray(a,ind) === 0) console.log("Не больше своих соседей");
        else if(ChecksInArray(a,ind) === -1) console.log("Сосед отсутствует");
        else console.log("Индекс невходит в область массива");
    }
})();

console.log("\ntask 7.\n");
function IndexOfLargeNeighbors(ar){
    for(var i = 0; i < ar.length; i++){
        if(ChecksInArray(ar,i) === 1) return i;
    }
    return -1
}
a = [];
for(var i = 0; i < Math.floor(Math.random()*8 + 1); i++) a[i] = Math.floor(Math.random()*7);
var index = IndexOfLargeNeighbors(a);
console.log("array: " + a);
console.log("array: " + a);
if(index !== -1) console.log("Индекс элемента, который больше своих родителей: " + index);
else console.log("Элемент не найден");

