'use strict'
console.log("task 1.\n");
var array = [];
for(var i = 0; i < 20; i++) array[i] = i*5;
console.log(array);

console.log("\ntask 2.\n");
var arrayCharOne = ['r','t','q','h','x','a'];
var arrayCharTwo = ['b','r','i','z','x'];
console.log(arrayCharOne);
console.log(arrayCharTwo);
for(var i = 0; i < arrayCharOne.length && i < arrayCharTwo.length; i++)
{
    if(arrayCharOne[i] > arrayCharTwo[i]) console.log(arrayCharOne[i] + " > " + arrayCharTwo[i]);
    else if(arrayCharOne[i] < arrayCharTwo[i]) console.log(arrayCharOne[i] + " < " + arrayCharTwo[i]);
    else console.log(arrayCharOne[i] + " = " + arrayCharTwo[i]);
}

console.log("\ntask 3.\n");
array = [];
for(var i = 0; i < 8; i++) array[i] = Math.floor(Math.random()*4);
console.log(array);
var maxCount = 0, count = 1;
for(var i = 1; i < 8; i++)
{
    if(array[i] == array[i-1]) count++;
    if(array[i] != array[i-1] || i == 7)
    {
        if(maxCount < count) maxCount = count;
        count = 1;
    }
}
count = 1;
for(var i = 1; i < 8; i++)
{
    if(array[i] == array[i-1]) count++;
    if(array[i] != array[i-1] || i == 7)
    {
        if(maxCount == count)
        {
            var a;
                if(i == 7 && array[i] == array[i-1]) a = array.slice(i-count+1, i+1);
                else a = array.slice(i-count, i);
            console.log(a);
        }
        count = 1;
    }
}

console.log("\ntask 4.\n");
array = [];
for(var i = 0; i < 8; i++) array[i] = Math.floor(Math.random()*7);
console.log(array);
maxCount = 0;
count = 1;
for(var i = 1; i < 8; i++)
{
    if(array[i] == array[i-1]+1) count++;
    if(array[i] != array[i-1]+1 || i == 7)
    {
        if(maxCount < count) maxCount = count;
        count = 1;
    }
}
count = 1;
for(var i = 1; i < 8; i++)
{
    if(array[i] == array[i-1]+1) count++;
    if(array[i] != array[i-1]+1 || i == 7)
    {
        if(maxCount == count)
        {
            var a;
            if(i == 7 && array[i] == array[i-1]+1) a = array.slice(i-count+1, i+1);
            else a = array.slice(i-count, i);
            console.log(a);
        }
        count = 1;
    }
}

console.log("\ntask 5.\n");
array = [];
for(var i = 0; i < 8; i++) array[i] = Math.floor(Math.random()*7);
console.log(array);
for(var i = 0; i < 8; i++)
{
    var min = array[i], minIndex = i;
    for(var j = i; j < 8; j++)
    {
        if(min > array[j]) { min = array[j]; minIndex = j; }
    }
    var tmp = array[minIndex];
    array[minIndex] = array[i];
    array[i] = tmp;
}
console.log(array);

console.log("\ntask 6.\n");
array = [];
for(var i = 0; i < 8; i++) array[i] = Math.floor(Math.random()*7);
console.log(array);
var uniqNamArray = [];
for(var i = 0; i < 8; i++)
{
    var haveNum = false;
    for(var j = 0; j < uniqNamArray.length; j++) if(uniqNamArray[j] == array[i]) haveNum = true;
    if(!haveNum) uniqNamArray.push(array[i]);
}
count = 0; maxCount = 0;
var maxCountNum;
for(var i = 0; i < uniqNamArray.length; i++)
{
    for(var j = 0; j < 8; j++) if(uniqNamArray[i] == array[j]) count++;
    if(maxCount < count)
    {
        maxCount = count;
        maxCountNum = uniqNamArray[i];
    }
    count = 0;
}
console.log("number " +  maxCountNum + " meet " + maxCount + " times");

console.log("\ntask 7.\n");
array = [1,1,2,3,5,5,8,9,10,12,14];
console.log(array);
var key = 8, first = 0, last = array.length, mid;
while (first < last)
{
    mid = Math.floor((first + last)/2);
    if(array[mid] >= key) last = mid;
    else first = ++mid;
}
if(array[mid] == key) console.log("index of key " + key + " is " + mid);
else console.log("key don't find");
