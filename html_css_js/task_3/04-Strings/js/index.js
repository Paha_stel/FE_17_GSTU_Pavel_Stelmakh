'use strict'
console.log("task 1.\n");
function Reverse(str) {
    return str.split('').reverse().join('');
}
var string = "Hello";
console.log("Befor reverse: " + string);
console.log("After reverse: " + Reverse(string));

console.log("\ntask 2.\n");
function CheckParentheses(str) {
    var count = 0;
    for(var i = 0; i < str.length; i++){
        if(str[i] == '(') count++;
        if(str[i] == ')') count--;
        if(count < 0) return false;
    }
    if(count == 0) return true;
    else return false;
}
var strFalse = "2*)(3+4*(3+4)";
var srtTrue = "2*(3+4*(3+4))"
console.log("выражение \"" + strFalse + "\" " + CheckParentheses(strFalse));
console.log("выражение \"" + srtTrue + "\" " + CheckParentheses(srtTrue));

console.log("\ntask 3.\n");
function CountSubString(str) {
    return str.match(/in/ig).length;
}
var str = "We are liv<b>in</b>g **in** an yellow submar<b>in</b>e. " +
    "We don't have anyth<b>in</b>g else. **In**side the submar<b>in</b>e is very tight. " +
    "So we are dr<b>in</b>k<b>in</b>g all the day. We will move out of it **in** 5 days";
console.log("Исходный текст:\n" + str);
console.log("Количество вхождений: " + CountSubString(str));

console.log("\ntask 4.\n");
function ChechangesText(str) {
    var strReg1 = str.match(/<upcase>[^<]*<\/upcase>/ig);
    var strReg2 = str.match(/<lowcase>[^<]*<\/lowcase>/ig);
    var strReg3 = str.match(/<mixcase>[^<]*<\/mixcase>/ig);
    for(var i = 0; i < strReg1.length; i++){
        strReg1[i] = strReg1[i].replace("<upcase>",''); strReg1[i] = strReg1[i].replace("</upcase>",'');
        str = str.replace("<upcase>" + strReg1[i] + "</upcase>", strReg1[i].toUpperCase());
    }
    for(var i = 0; i < strReg2.length; i++){
        strReg2[i] = strReg2[i].replace("<lowcase>",''); strReg2[i] = strReg2[i].replace("</lowcase>",'');
        str = str.replace("<lowcase>" + strReg2[i] + "</lowcase>", strReg2[i].toLowerCase());
    }
    for(var i = 0; i < strReg3.length; i++){
        strReg3[i] = strReg3[i].replace("<mixcase>",''); strReg3[i] = strReg3[i].replace("</mixcase>",'');
        var tmp = strReg3[i].split('');
        for(var j = 0; j < tmp.length; j++) {
            if (Math.round(Math.random())) tmp[j] = tmp[j].toUpperCase();
            else tmp[j] = tmp[j].toLowerCase();
        }
        tmp = tmp.join('');
        str = str.replace("<mixcase>" + strReg3[i] + "</mixcase>", tmp);
    }
    return str;
}
str = "We are <mixcase>living</mixcase> in a <upcase>yellow submarine</upcase>." +
        "We <mixcase>don't</mixcase> have <lowcase>anything</lowcase> else."
console.log("Исходный текст:\n" + str);
str = ChechangesText(str);
console.log("Измененный текст:\n" + str);

console.log("\ntask 5.\n");
function ReplaceSpace(str) {

    return str.replace(/\s/g, '&nbsp;');
}
var str = "Hello, my name Pashka";
console.log("Исходная строка: " + str);
console.log("Измененная строка: " + ReplaceSpace(str));

console.log("\ntask 6.\n");
function InnetHTML() {
    var textTitile = document.getElementsByTagName("title")[0].innerText;
    var textHtml = document.getElementsByTagName("html")[0].innerText;
    textHtml = textHtml.replace(/\n/g,'');
    return textTitile + textHtml;
}
console.log(InnetHTML());

console.log("\ntask 7.\n");
function URLJSON(url) {
    var urlArray = url.match(/([^:]*):\/\/(www.[^/]*)(.*)/i);
    var urlObj = {
        "protocol": urlArray[1],
        "server": urlArray[2],
        "resource": urlArray[3]
    };
    return JSON.stringify(urlObj);
}
var URL = "http://www.devbg.org/forum/index.php";
console.log(URL);
console.log(URLJSON(URL));

console.log("\ntask 8.\n");
(function () {
    var a = document.getElementsByTagName("a");
    var length = a.length;
    for(var i = 0; i < length; i++) {
        a = document.getElementsByTagName("a");
        var url = a[0].parentNode.innerHTML.match(/<a href=\"([^\"]*)/);
        a[0].parentNode.innerHTML = a[0].parentNode.innerHTML.replace("<\/a>", "[\/URL]");
        a[0].parentNode.innerHTML = a[0].parentNode.innerHTML.replace("<a href=\"" + url[1] + "\">", "[URL = " + url[1] + "]");
    }
})()

console.log("\ntask 9.\n");
function EmailArray(text) {
    return text.match(/[a-z]*\@[a-z]*\.[^\s]*/ig);
}
var text = "my address pavelstelmax@rambler.ru and pavelStelmah@yandex.by";
console.log("Исходная строка: " + text);
console.log("Электронные почты: " + EmailArray(text));

console.log("\ntask 10.\n");
function Palindromes(text) {
    var words = text.split(/ |\?|,/);
    var palindr = [];
    for(var i = 0, j = 0; i < words.length; i++){
        if(words[i].length > 1 && words[i] == words[i].split('').reverse().join('')){
            palindr[j] = words[i];
            j++;
        }
    }
    return palindr;
}
text = "bla - bala ABBA? no, bla bla lamal and exe";
console.log("Исходная строка: " + text);
console.log("Палиндромы: " + Palindromes(text));

console.log("\ntask 11.\n");
function StringFormat() {
    for(var i = 1; i < arguments.length; i++){
        if(arguments[0].replace(new RegExp("\\{" + (i-1) + "\\}"), arguments[i]))
            arguments[0] = arguments[0].replace(new RegExp("\\{" + (i-1) + "\\}",'g'), arguments[i]);
    }
    return arguments[0];
}
var str = "Hello {0}, my name is {1}. I love this {0}. Here I am {2}";
console.log("Исходная строка: " + str);
console.log("После заполнения: " + StringFormat(str, "word", 2626, "gad"));

console.log("\ntask 12.\n");
function generateList(people, template) {
    var templ = "<ul>";
    for(var i = 0; i < people.length; i++){
        templ += "<li>" + template.trim() + "</li>";
        for(var j in people[i])
            templ = templ.replace("-{" + j + "}-", people[i][j]);
    }
    templ = templ + "</ul>";
    return templ;
}
var people = [{name: "Peter", age: 14},{name: "Naruto", age: 10}];
var tmpl = document.getElementById("list-item").innerHTML;
document.getElementById("list-item").innerHTML = generateList(people, tmpl);
