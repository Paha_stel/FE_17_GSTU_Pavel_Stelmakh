'use strict'
console.log("task 1.\n");
function Modul(p1,p2) {
    return Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
}
function IsTriangle(l1,l2,l3) {
    var pointArray = [l1.p1, l1.p2, l2.p1, l2.p2, l3.p1, l3.p2];
    var tmp = [];
    var have;
    for(var  i = 0, k = 0; i < pointArray.length; i++){
        have = false;
        for(var j = 0; j < tmp.length; j++){
            if(pointArray[i].x == tmp[j].x && pointArray[i].y == tmp[j].y) have = true;
        }
        if(!have){ tmp[k] = pointArray[i]; k++; }
    }
    if(tmp.length == 3) return true;
    else return false;
}
var P1 = {x:0, y:0};
var P2 = {x:3, y:4};
console.log("point first " + P1.x + ", " + P1.y + "; point second " + P2.x + ", " + P2.y)
console.log("distance " + Modul(P1,P2));
var L1 = {
    p1: {x:0, y:0},
    p2: {x:3, y:0}
};
var L2 = {
    p1: {x:0, y:0},
    p2: {x:3, y:4}
};
var L3 = {
    p1: {x:3, y:0},
    p2: {x:3, y:4}
};
console.log("Line 1: [(" + L1.p1.x + ", " + L1.p1.y + "), (" + L1.p2.x + ", " + L1.p2.y + ")]");
console.log("Line 2: [(" + L2.p1.x + ", " + L2.p1.y + "), (" + L2.p2.x + ", " + L2.p2.y + ")]");
console.log("Line 3: [(" + L3.p1.x + ", " + L3.p1.y + "), (" + L3.p2.x + ", " + L3.p2.y + ")]");
if(IsTriangle(L1,L2,L3)) console.log("Данные линии могут образовывать треугольник");
else console.log("Данные линии не могут образовывать треугольник");


console.log("\ntask 2.\n");
var rem = function (delKey) {
    for(var i = 0; i < this.length; i++){
        if(this[i] === delKey){
            this.splice(i,1);
        }
    }
    return this;
}
var arr = [1, 2, 1, 4, 1, 3, 4, 1, 111, 3, 2, 1, "1"];
console.log(arr);
Array.prototype.remove = rem;
arr = arr.remove(1);
console.log(arr);

console.log("\ntask 3.\n");
function Copy(a) {
    if(typeof a == "object"){
        var copy = {};
        for(var i in a) copy[i] = a[i];
        return copy;
    }
    else return a;
}
var person = {
    name: "Pasha"
};
console.log("Поле name до копирования: " + person.name);
var personCopy = Copy(person);
console.log("Поле name после копирования: " + person.name);
personCopy.name = "Sasha";
console.log("Измененное поле name копии: " + personCopy.name);
var num = 3;
var numCopy = Copy(num);
console.log("Приметивное копирование числа: " + num);
console.log("Копия числа: " + numCopy);

console.log("\ntask 4.\n");
function hasProperty(obj, property) {
    for(var i in obj) if(i == property) return true;
    return false;
}
var obj = {
    name: "Pasha",
    surname: "Stelmakh"
};
console.log(obj);
console.log("свойство \"name\" в объекте: " + hasProperty(obj, "name"));
console.log("свойство \"length\" в объекте: " + hasProperty(obj, "length"));

console.log("\ntask 5.\n");
function FindYoungestPerson(persons) {
    var ageMin = persons[0].age, indexMin = 0;
    for(var i = 1; i < persons.length; i++){
        if(persons[i].age < ageMin){
            ageMin = persons[i].age;
            indexMin = i;
        }
    }
    return persons[indexMin];
}
var persons = [
    { firstName : "Gosho", lastName: "Petrov", age: 32 },
    { firstName : "Bay", lastName: "Ivan", age: 81 },
    {firstName : "Pasha", lastName: "Stelmakh", age: 100},
    {firstName : "Sasha", lastName: "Stelmax", age: 22}
];
var youngestPerson = FindYoungestPerson(persons);
for(var i = 0; i < persons.length; i++) {
    console.log(persons[i]);
}
console.log("Самый молодой: " + youngestPerson.firstName + " " + youngestPerson.lastName);

console.log("\ntask 6.\n");
function group(persons, key) {
    var grp = {};
    var have;
    if(key == "age") {
     for (var i = 0; i < persons.length; i++) {
      have = false;
            for(var j in grp){
                if(persons[i].age == j){ have = true; grp[persons[i].age].push(persons[i]);}
            }
            if(!have) {
                grp[persons[i].age] = [];
                grp[persons[i].age].push(persons[i]);
            }
        }
    }
    if(key == "firstName") {
        for (var i = 0; i < persons.length; i++) {
            have = false;
            for(var j in grp){
                if(persons[i].firstName == j){ have = true; grp[persons[i].firstName].push(persons[i]);}
            }
            if(!have) {
                grp[persons[i].firstName] = [];
                grp[persons[i].firstName].push(persons[i]);
            }
        }
    }
    if(key == "lastName") {
        for (var i = 0; i < persons.length; i++) {
            have = false;
            for(var j in grp){
                if(persons[i].lastName == j){ have = true; grp[persons[i].lastName].push(persons[i]);}
            }
            if(!have) {
                grp[persons[i].lastName] = [];
                grp[persons[i].lastName].push(persons[i]);
            }
        }
    }
    return grp;
}
var persons = [
    {age: 20, firstName: "Peter", lastName: "Ivanov"},
    {age: 40, firstName: "Peter", lastName: "Vaider"},
    {age: 30, firstName: "Peter", lastName: "Ivanov"},
    {age: 40, firstName: "Dart", lastName: "Vaider"},
    {age: 20, firstName: "Dart", lastName: "Ivanov"},
    {age: 30, firstName: "Dart", lastName: "Vaider"}
    ];
console.log("All people:");
for(var i = 0; i < persons.length; i++){
    console.log(persons[i]);
}
console.log("Group of people by Age: ");
var groupedByAge = group(persons, "age");
for(var i in groupedByAge){
    console.log(i + ":"); console.log(groupedByAge[i]);
}
console.log("Group of people by FirstName: ");
var groupedByFirstName = group(persons, "firstName");
for(var i in groupedByFirstName){
    console.log(i + ":"); console.log(groupedByFirstName[i]);
}
console.log("Group of people by LastName: ");
var groupedByLastName = group(persons, "lastName");
for(var i in groupedByLastName){
    console.log(i + ":"); console.log(groupedByLastName[i]);
}