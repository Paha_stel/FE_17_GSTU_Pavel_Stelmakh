'use strict'
var text = document.querySelector('[type="text"]');
var but = document.querySelector('[type="submit"]');
var digit = (Math.round(1000-0.5+Math.random()*(9999-1000))).toString();
var game = document.querySelector('div.game');
var win = false
var n = 0;
console.log(digit);
function Rams(val) {
    var n = 0;
    for(var i = 0; i < val.length; i++){
        if(val[i] == digit[i]) n++;
    }

    return n;
}
function Sheeps(val) {
    var n = 0;
    for(var i = 0;i < val.length; i++){
        for(var j = 0; j < digit.length; j++){
            if(val[i] == digit[j]) n++;
        }
    }
    return n;
}

but.onclick = function () {
    var value = text.value; n++;
    if(value.match(/\d{4}/i) !== null && value.length == 4 && !win){
        var div = document.createElement('div');
        div.innerHTML = "<div>" + value + "</div><div>s.: " + Sheeps(value) + ", r.:" + Rams(value) + "</div>" ;
        div.className = "result";
        div.style.height = "0px";
        game.insertBefore(div, document.querySelector('div.input').nextSibling);
        setTimeout(function () { div.style.height = "40px"; },10);
        setTimeout(function () { div.style.color = "black"; },110);
        if(Rams(value) == 4){
            div.className += " win";
            div.innerHTML += "<div>WIN</div>";
            win = true;
            setTimeout(Save, 2000);
        }

    }
    else if(!win) alert("Введите четырехзначное число!");
};

function Save() {
    Clear();
    Input();
}
function Clear() {;
    var child =  game.childNodes.length;
    for(var i = 0; i < child; i++)
        if(game.childNodes[0] !== undefined) game.removeChild(game.childNodes[0]);
}
function Input() {
    var div = document.createElement('div');
    div.className = "input";
    text = document.createElement('input');
    but = document.createElement('input');
    text.type = "text"; text.value = "Введите ник";
    but.type = "submit"; but.value = "отправить";
    div.appendChild(text); div.appendChild(but);
    game.appendChild(div);
    but.onclick = function () {
        var value = text.value;
        if(value != ""){
            var v = localStorage.getItem("game");
            if(v == null)localStorage.setItem("game",value + "," + n.toString());
            else{
                v += ";"+value + "," + n.toString();
                localStorage.setItem("game",v);
            }
            Clear();
            Table();
        }
        else alert("Введите имя");
    }
}
function Table() {
    var data = localStorage.getItem("game").split(';');
    var div = document.createElement('div');
    div.className = "input";
    but = document.createElement('input');
    but.type = "submit"; but.value = "очистить";
    div.appendChild(but);
    but.onclick = function () {
        localStorage.clear();
        var child =  game.childNodes.length;
        for(var i = 0; i < child; i++) {
            var nd = game.childNodes;
            if (nd[nd.length - 1] !== undefined && nd[nd.length - 1].className != "input")
                game.removeChild(nd[nd.length - 1]);
        }
    }
    game.appendChild(div);
    var mIndex = 0;
    var divWins = [];
    var j = 0;
    for(var i = 0; i < data.length; i++){
        var nameAndCount = data[i].split(',');
        var divRes = document.createElement('div');
        divRes.innerHTML = "<div>" + nameAndCount[0] + "</div><div>" + nameAndCount[1] + "</div>" ;
        divRes.className = "result";
        divRes.style.height = "40px";
        divRes.style.color = "black";
        game.insertBefore(divRes, document.querySelector('div.input').nextSibling);
        if(Number(data[mIndex].split(',')[1]) >= Number(nameAndCount[1])) {
            if(Number(data[mIndex].split(',')[1]) > Number(nameAndCount[1])) {
                j = 0; divWins = [];
            }
            mIndex = i;
            divWins[j] = divRes; j++
        }
    }
    for(j = 0; j < divWins.length; j++)
        divWins[j].className += " win";
}