'use strict'
console.log("-------------- 1. ----------------");
function FindDiv() {
    var divs = [];
    var div = document.getElementsByTagName('div');
    for(var i = 0,j = 0; i < div.length; i++){
        if(div[i].querySelectorAll('div')[0] !== undefined) {
            divs[j] = div[i].querySelectorAll('div')[0];
            j++;
        }
    }
    console.log("count tags: " + divs.length + "\n");
    console.log(divs);
}
FindDiv();
console.log("-------------- 2. ----------------");
(function () {
    var input = document.getElementsByTagName('input');
    for(var i = 0; i < input.length; i++){
        if(input[i].getAttribute('type') == "text"){
            //console.log(input[i].getAttribute('value'));
            console.log(input[i].value);
        }
    }
})();
console.log("-------------- 3. ----------------");
(function () {
    var input = document.getElementsByTagName('input');
    for(var i = 0; i < input.length; i++){
        if(input[i].getAttribute('type') == "color") document.getElementsByTagName('body')[0].style.backgroundColor = input[i].getAttribute('value');
    }
})();