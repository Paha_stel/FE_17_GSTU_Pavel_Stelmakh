var text = document.getElementsByTagName('textarea')[0];
var inFont = document.querySelector('.font');
var inBg = document.querySelector('.bg');


inFont.addEventListener("change", HFont);
function HFont(ev) {
    text.style.color = ev.target.value;
}

inBg.addEventListener("change", HBg);
function HBg(ev) {
    text.style.backgroundColor = ev.target.value;
}

