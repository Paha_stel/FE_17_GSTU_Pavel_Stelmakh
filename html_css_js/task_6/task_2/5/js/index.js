'use strict'

var html = document.querySelector('html');
var sourse = document.querySelector('body>div>div');

var divP = document.createElement('div');
divP.className = 'private';
sourse.appendChild(divP);
var dhtml = document.createElement('div');
dhtml.innerHTML = "html<br>";
var j = 0;
dhtml.className = j;
dhtml.setAttribute('act','');
dhtml.onclick = function (){ ChldElem(html, this); };
divP.appendChild(dhtml);
var tags = [];
tags[j] = html;
j++;

function ChldElem(div, th) {
    if(div.getAttribute('act') != 'active') {
        var child = div.childNodes;
        var strStar = "*", tempChild = div;
        while (tempChild != html) {
            tempChild = tempChild.parentNode;
            strStar += "*";
        }
        for (var i = 0, k = 0; i < child.length; i++) {
            if (child[i].localName !== undefined && child[i].className != 'private') {
                var chDiv = document.createElement('div');
                chDiv.innerHTML = strStar + child[i].localName + "<br>";
                chDiv.className = j;
                chDiv.setAttribute('act','');
                tags.splice(Number(th.className) + k + 1, 0, child[i]);
                chDiv.onclick = function () {
                    th = this;
                    ChldElem(tags[Number(this.className)], this);
                };
                divP.insertBefore(chDiv, divP.childNodes[Number(th.className) + k].nextElementSibling);

                k++;
                j++;
            }

        }
        for (i = 0; i < divP.childNodes.length; i++) {
            divP.childNodes[i].className = i;
        }
        div.setAttribute('act','active');
    }
    else{
        tags.splice(Number(th.className) + 1, Count(div));
        for(var i = 0; i < Count(div); i++){
            divP.removeChild(divP.childNodes[Number(th.className)+1]);
        }
        div.setAttribute('act','');
        for (i = 0; i < divP.childNodes.length; i++) {
            divP.childNodes[i].className = i;
        }
    }
}

function Count(div) {
    var count = 0;
    for(var i = 0; i < div.childNodes.length; i++){
        if (div.childNodes[i].localName !== undefined && div.childNodes[i].className != 'private') count++;
    }
    return count;
}
