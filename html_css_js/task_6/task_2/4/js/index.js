'use strict'

function RandomInt(min,max) {
    var a = min - 0.5 + Math.random()*(max - min + 1);
    return Math.round(a);
}

function generateTagCloud(tags,min,max) {
    var cloud = document.createElement('div');
    cloud.style.width = (min+max)/2*tags.length/2 + "px";
    cloud.style.height = (min+max)/2*tags.length/2 + "px";
    cloud.style.border = "3px solid black";
    for(var i = 0; i < tags.length; i++){
        var span = document.createElement('span');
        var size = RandomInt(min, max);
        span.style.fontSize = size + "px";
        span.innerHTML = tags[i]+' ';
        span.style.height = size + RandomInt(10, 30) + "px";
        if(Math.round(Math.random())) span.style.verticalAlign = "top";
        cloud.appendChild(span);
    }
    return cloud;
}







var tags = [
    "cms", "javascript", "js", "ASP.NET MVC", ".net", ".net", "css", "wordpress", "xaml", "js", "http",
    "web", "asp.net", "asp.net MVC", "ASP.NET MVC", "wp", "javascript", "js", "cms", "html", "javascript",
    "http", "http", "CMS"
];

var tagCloud = generateTagCloud(tags, 17, 42);
document.querySelector('body').appendChild(tagCloud);