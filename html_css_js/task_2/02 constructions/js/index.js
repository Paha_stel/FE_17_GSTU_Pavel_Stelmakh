'use strict'
document.write("<hr><h3>Task 1</h3>");
var A = 3, B = 8;
var sum = 0, i = A;
while(i <= B){ sum += i; i++;}
document.write("<p>A = " + A + ", B = " + B + "<br>sum of numbers between A and B: " + sum + "</p>");
document.write("<p>odd numbers: ");
i = A;
while(i <= B){
    if(i % 2 !== 0) document.write(i + " ");
    i++;
}
document.write("</p>");
document.write("<hr><h3>Task 2</h3>");
var N = 5, fact = 1;
i = N;
do{
    fact *= i;
    i--;
} while(i > 0);
document.write("<p>count goods: " + N + "<br>factorial: " + fact + "</p>");
document.write("<hr><h3>Task 3</h3>");
var sp = " &nbsp";
var star = "*";
document.write("<p>");
var n= 5, m = 6;
//rectangle
for(var i = 0; i < n; i++)
{
    if(i == 0 || i == n-1){
        for(var j = 0; j < m; j++) document.write(star);
    }
    else {
        for(var j = 0; j < m; j++){
            if(j == 0 || j == m-1) document.write(star);
            else document.write(sp);
        }
    }
    document.write("<br>");
}
document.write("</p>");
//right triangle
document.write("<p>");
for(var i = 0; i < n; i++)
{
    if(i == 0 || i == n-1){
        for(var j = 0; j <= i; j++) document.write(star);
    }
    else {
        for(var j = 0; j <= i; j++){
            if(j == 0 || j == i) document.write(star);
            else document.write(sp);
        }
    }
    document.write("<br>");
}
document.write("</p>");
//equilateral triangle
document.write("<p>");
var q = 0;
for(var i = 0; i < n; i++)
{
    if(i == 0 || i == n-1) {
        if(i == 0){
            for(var j = 0; j < n; j++) document.write(sp);
            document.write(star);
        }
        else{
            for(var j = 0; j < 2*n; j++){
                if(j % 2) document.write(sp);
                else document.write(star);
            }
        }
    }
    else{
        for(var j = 0; j < 2*n; j++) {
        if (j == n-q || j== n+q) {
            document.write(star);
        }
        else {
              document.write(sp);
            }
        }
    }
    q++;
    document.write("<br>");
}
document.write("</p>");
//rhombus
document.write("<p>");
q = 0;
for(var i = 0; i < n; i++)
{
    if(i == 0) {
        for(var j = 0; j < n; j++) document.write(sp);
        document.write(star);
    }
    else{
        for(var j = 0; j < 2*n; j++) {
            if (j == n-q || j== n+q) {
                document.write(star);
            }
            else {
                document.write(sp);
            }
        }
    }
    q++;
    document.write("<br>");
}
q -= 2;
for(var i = 0; i < n-1; i++)
{
    if(i == n-2) {
        for(var j = 0; j < n; j++) document.write(sp);
        document.write(star);
    }
    else{
        for(var j = 0; j < 2*n; j++) {
            if (j == n-q || j== n+q) {
                document.write(star);
            }
            else {
                document.write(sp);
            }
        }
    }
    q--;
    document.write("<br>");
}
document.write("</p>");