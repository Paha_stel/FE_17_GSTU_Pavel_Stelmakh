'use strict'
document.write("<hr><h3>Task 1</h3>");
var ar = [2,4,-2,1,-5,-3,9,6,5,1,4];
document.write("<p>Array: " + ar + "</p>");
var maxValue = ar[0],minValue = ar[0], middleValue, sum = 0;
for(var i in ar){
    sum += ar[i];
    if(maxValue < ar[i]) maxValue = ar[i];
    if(minValue > ar[i]) minValue = ar[i];
}
middleValue = sum/ar.length;
document.write("<p>");
document.write("Max Value: " + maxValue + "<br>");
document.write("Min Value: " + minValue + "<br>");
document.write("Middle Value: " + middleValue + "<br>");
document.write("Sum: " + sum + "<br>");
document.write("Odd Numbers: ");
for(var i in ar){
    if(ar[i] % 2) document.write(ar[i] + " ");
}
document.write("</p>");
document.write("<hr><h3>Task 2</h3>");
ar = [
    [1,4,-2,4,0],
    [-2,-3,1,0,5],
    [0,0,6,4,2],
    [0,4,1,-1,3],
    [1,1,1,1,1]
];
document.write("<p>");
document.write("Before: <br>");
for(var i in ar){
    document.write(ar[i] + "<br>");
}
document.write("After: <br>");
for(var i = 0; i < ar.length; i++){
    if(ar[i][i] >= 0) ar[i][i] = 1;
    else ar[i][i] = 0;
}
for(var i in ar){
    document.write(ar[i] + "<br>");
}
document.write("</p>");