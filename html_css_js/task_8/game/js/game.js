document.getElementsByTagName('body')[0].removeChild(document.getElementsByTagName('div')[0]);
var canvas = document.createElement('canvas');
document.getElementsByTagName('body')[0].insertBefore(canvas,document.getElementsByTagName('script')[0]);
canvas.innerHTML = "Данный тег не поддерживается";
var width = document.documentElement.clientWidth;
var height = document.documentElement.clientHeight;
canvas.setAttribute('width', width);
canvas.setAttribute('height', height);
var cont = canvas.getContext('2d');
var FPS = 100;
var busket = new Image();
busket.src = "img/busket.png";
busket.onload = function () {
    cont.drawImage(busket,20,60,80,80);
}
var open = 1;
var Cap = function() {
    this.X =  21;
    this.Y =  43;
    this.OldX =  this.X;
    this.OldY =  this.Y;
    this.Size1 = 80;
    this.Size2 = 25;
    this.Img = new Image();
}

Cap.prototype = {
    Load: function () {
        var img = this.Img;
        var x = this.X, y = this.Y, size1 = this.Size1, size2 = this.Size2;
        img.src = "img/cap.png";
        img.onload = function () { cont.drawImage(img,x,y,size1,size2); };
    },
    Draw: function () {
        var img = this.Img;
        var x = this.X, y = this.Y, size1 = this.Size1, size2 = this.Size2;
        cont.drawImage(img,x,y,size1,size2);
    },
    Clear: function (up) {
        cont.save();
        if(up === true) {
            cont.translate(-23,-65);
            cont.rotate(0.2);
            cont.clearRect(this.X, this.Y, this.Size1, this.Size2);
            cont.translate(23,65);
        }
        else {
            cont.translate(-28,-70);
            cont.rotate(-0.2);
            cont.clearRect(this.X-12,this.Y+3,this.Size1,this.Size2);
            cont.translate(28,70);
        }
        cont.restore();
    }
};
var cap = new Cap();
var alfa = 0,idUp,idDown,idDestr;

function IntRandom(min, max) {
    var r = min - 0.5 + Math.random()*(max - min);
    return Math.round(r);
}

var DrawTrash = function() {
    this.X =  IntRandom(100,400);
    this.Y =  IntRandom(100,400);
    this.OldX =  this.X;
    this.OldY =  this.Y;
    this.Size = 50;
    this.Img = new Image();
    this.Select = false;
}

DrawTrash.prototype = {
    Load: function () {
        var img = this.Img;
        var x = this.X, y = this.Y, size = this.Size;
        img.src = "img/trash.png";
        img.onload = function () { cont.drawImage(img,x,y,size,size); };
    },
    Draw: function () {
        var img = this.Img;
        var x = this.X, y = this.Y, size = this.Size;
        cont.drawImage(img,x,y,size,size);
    },
    Clear: function () {
        if(this.Select === true){
            cont.clearRect(this.OldX,this.OldY,this.Size,this.Size);
        }
        else cont.clearRect(this.X,this.Y,this.Size,this.Size);
    },
    Selected: function () {
        this.Select = !this.Select;
    }
};
var trashes = [new DrawTrash(),new DrawTrash(),new DrawTrash(),new DrawTrash(),new DrawTrash(),new DrawTrash()];
for(var i in trashes) trashes[i].Load();
cap.Load();
var main = setInterval(function () {
    for(var i in trashes){
        trashes[i].Clear();
    }
    for(var i in trashes) trashes[i].Draw();
    cont.drawImage(busket,20,60,80,80);
    cont.save();
    cont.translate(28,70);
    if(open == 1) {
        cont.rotate(-alfa);
    }
    else {
        cont.rotate(-alfa);
    }
    cont.translate(-28,-70);
    cap.Draw();
    cont.restore();


},1000/FPS);
function MouseInRect(x,y,obg) {
    return x >= obg.X && x <= obg.X + obg.Size && y >= obg.Y && y <= obg.Y + obg.Size
}
function MDown(ev) {
    for(var i in trashes){
        if (MouseInRect(ev.pageX, ev.pageY, trashes[i])){ trashes[i].Selected(); break;}
    }
}
function MUp(ev) {
    for(var i in trashes){
        if (trashes[i].Select === true){ trashes[i].Selected(); break;}
    }
}
function MouseMove(ev) {
    for(var i in trashes){
        if (trashes[i].Select === true){
            trashes[i].OldX = trashes[i].X;
            trashes[i].OldY = trashes[i].Y;
            trashes[i].X = ev.pageX - trashes[i].Size / 2;
            trashes[i].Y = ev.pageY - trashes[i].Size / 2;
            if(MouseInRect(trashes[i].X + trashes[i].Size / 2, trashes[i].Y+ trashes[i].Size / 2, {
                    X: cap.X - cap.Size1 / 2,
                    Y: cap.Y - cap.Size1 / 2,
                    Size: cap.Size1 + cap.Size1 / 2
                })) Throw(i);
            break;
        }
    }
}

function Throw(indx) {
    if(idUp === undefined) idUp = setInterval(UpCap,50);
    if(idDestr === undefined) idDestr = setTimeout(function(){
        Destroy();
    },1000);
    if(idDown === undefined) idDown = setTimeout(function () {
        idDown = setInterval(DownCap,50);
    },2000);
}

function UpCap() {
    if(alfa <= Math.PI / 2){
        alfa += 0.2;
        cont.save();
        cont.translate(28,70);
        cont.rotate(-alfa);
        cap.Clear(true);
        cont.translate(-28,-70);
        cont.restore();
    }
    else {
        clearInterval(idUp);
        open = 1;
    }
}
function DownCap() {
    if(alfa > 0.2){
        alfa -= 0.2;
        cont.save();
        cont.translate(28,70);
        cont.rotate(-alfa);
        cap.Clear(false);
        cont.translate(-28,-70);
        cont.restore();
    }
    else {
        clearInterval(idDown);
        open = 0;
        idUp = undefined;
        idDestr = undefined;
        idDown = undefined;
        if(trashes.length == 0) End();
    }
}

function Destroy() {
    for(var i in trashes){
        if (trashes[i].Select === true || MouseInRect(trashes[i].X + trashes[i].Size / 2, trashes[i].Y+ trashes[i].Size / 2, {
                X: cap.X - cap.Size1 / 2,
                Y: cap.Y - cap.Size1 / 2,
                Size: cap.Size1 + cap.Size1 / 2
            })){
            delete trashes[i];
            break;
        }
    }
    var defTrashes = [];
    var j = 0;
    for(var i in trashes){
        if (trashes[i] !== undefined){
            defTrashes[j] = trashes[i];
            j++;
        }
    }
    trashes = defTrashes;
}

function End() {
    clearInterval(main);
    var script = document.createElement('script');
    script.src = "js/end.js";
    script.type = "text/javascript";
    document.getElementsByTagName('body')[0].appendChild(script);
}

canvas.onmousedown = MDown;
canvas.onmouseup = MUp;
canvas.onmousemove = MouseMove;
