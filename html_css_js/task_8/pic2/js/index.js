'use strict'
var canvas = document.getElementById('canvas');
//canvas.style.border = "1px solid black";
var cont = canvas.getContext('2d');

cont.beginPath();//колеса
cont.lineWidth = 5;
cont.strokeStyle = "rgb(0,0,0)";
cont.fillStyle = "rgb(30,30,30)";
cont.arc(200,400,80,0,2*Math.PI,true);
cont.fill();
cont.stroke();
cont.closePath();
cont.beginPath();
cont.arc(530,400,80,0,2*Math.PI,true);
cont.fill();
cont.stroke();
cont.closePath();

cont.beginPath();//рама
cont.moveTo(200,400);
cont.lineTo(350,400);
cont.lineTo(500,300);
cont.lineTo(320,300);
cont.lineTo(200,400);
cont.moveTo(500,300);
cont.lineTo(530,400);
cont.stroke();
cont.closePath();

cont.beginPath();//руль
cont.moveTo(500,300);
cont.lineTo(470,210);
cont.moveTo(420,250);
cont.lineTo(470,210);
cont.lineTo(420,180);
cont.stroke();
cont.closePath();

cont.beginPath();//седушка
cont.moveTo(315,270);
cont.lineTo(350,400);
cont.moveTo(290,270);
cont.lineTo(340,270);
cont.stroke();
cont.closePath();

cont.beginPath();//педали
cont.arc(350,400,20,0,2*Math.PI,true);
cont.moveTo(335,390);
cont.lineTo(310,370);
cont.moveTo(365,415);//изменять
cont.lineTo(385,435);
cont.stroke();
cont.closePath();

