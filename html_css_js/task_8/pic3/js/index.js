'use strict'
var canvas = document.getElementById('canvas');
//canvas.style.border = "1px solid black";
var cont = canvas.getContext('2d');

cont.beginPath();//здание
cont.fillStyle = "rgb(100,100,100)";
cont.fillRect(250,450,600,340);
cont.moveTo(550,100);
cont.lineTo(250,445);
cont.lineTo(850,445);
cont.lineTo(550,100);
cont.fill();
cont.closePath();

cont.beginPath();//окна
function Wind(x,y) {
    cont.clearRect(x,y,100,50);
    cont.clearRect(x + 105,y,100,50);
    cont.clearRect(x,y + 55,100,50);
    cont.clearRect(x + 105,y + 55,100,50);
}
Wind(300,500);
Wind(600,500);
Wind(600,650);
cont.closePath();

cont.beginPath();//труба
cont.lineWidth = 3;
cont.fillStyle = "rgb(100,100,100)";
cont.fillRect(680,160,50,200);
cont.strokeStyle = "rgb(70,70,70)";
cont.lineCap = "round";
cont.moveTo(680,158);
cont.lineTo(680,360);
cont.moveTo(730,158);
cont.lineTo(730,360);
cont.stroke();
cont.closePath();
cont.beginPath();
cont.ellipse(705,158,25,7,0,0,2*Math.PI,true);
cont.fill();
cont.stroke();
cont.closePath();

cont.beginPath();//двери
cont.strokeStyle = "rgb(255,255,255)";
cont.moveTo(330,790);
cont.lineWidth = 3;
cont.lineTo(330,680);
cont.moveTo(475,790);
cont.lineTo(475,680);
cont.moveTo(330,680);
cont.bezierCurveTo(370,620,435,620,475,680);
cont.moveTo(402,790);
cont.lineTo(402,636);
cont.stroke();
cont.closePath();
cont.beginPath();
cont.arc(380,730,5,0,2*Math.PI,true);
cont.stroke();
cont.closePath();
cont.beginPath();
cont.arc(425,730,5,0,2*Math.PI,true);
cont.stroke();
cont.closePath();