'use strict'
var canvas  = document.getElementById("canvas");
//canvas.style.border = "1px solid black";
var ctx     = canvas.getContext('2d');
ctx.beginPath();
ctx.fillStyle = "rgb(134,80,88)";
ctx.scale(1,0.9);
ctx.arc(400, 670, 210, 0, 2*Math.PI, false);
ctx.fill();
ctx.closePath();
ctx.beginPath();
ctx.fillStyle = "rgb(144,0,88)";
ctx.arc(400, 670, 200, 0, 2*Math.PI, false);
ctx.fill();
ctx.closePath();
ctx.beginPath();
ctx.lineWidth = 5;
ctx.arc(400, 670, 160, 0, Math.PI, false);
ctx.stroke();
ctx.closePath()
ctx.beginPath();
ctx.fillStyle = "rgb(0,0,0)";
ctx.scale(1,0.2);
ctx.arc(400,2400,240,0,2*Math.PI,false);
ctx.fill();
ctx.closePath();
ctx.beginPath();
ctx.fillStyle = "rgb(40,40,240)";
ctx.arc(400,2400,230,0,2*Math.PI,false);
ctx.fill();
ctx.scale(1,4);
ctx.closePath();
ctx.beginPath();
ctx.moveTo(320,300);
ctx.lineTo(320,545);
ctx.quadraticCurveTo(400,580,480,545);
ctx.lineTo(480,300);
ctx.quadraticCurveTo(400,335,320,300);
ctx.quadraticCurveTo(400,265,480,300);
ctx.quadraticCurveTo(400,335,320,300);
ctx.fillStyle = "rgb(0,0,0)";
ctx.fill();
ctx.closePath();
ctx.beginPath();
ctx.moveTo(330,310);
ctx.lineTo(330,535);
ctx.quadraticCurveTo(390,570,470,535);
ctx.lineTo(470,310);
ctx.quadraticCurveTo(390,325,330,310);
ctx.moveTo(330,300);
ctx.quadraticCurveTo(410,280,470,300);
ctx.quadraticCurveTo(410,320,330,300);
ctx.fillStyle = "rgb(40,40,240)";
ctx.fill();
ctx.closePath();
ctx.beginPath();//глаза
ctx.moveTo(260,720);//1
ctx.quadraticCurveTo(305,770,350,720);
ctx.quadraticCurveTo(305,660,260,720);
ctx.moveTo(450,720);//2
ctx.quadraticCurveTo(495,770,540,720);
ctx.quadraticCurveTo(495,660,450,720);
ctx.fillStyle = "rgb(0,0,0)";
ctx.fill();
ctx.closePath();
ctx.beginPath();
ctx.moveTo(265,720);//1
ctx.quadraticCurveTo(305,765,345,720);
ctx.quadraticCurveTo(305,665,265,720);
ctx.moveTo(455,720);//2
ctx.quadraticCurveTo(495,765,535,720);
ctx.quadraticCurveTo(495,665,455,720);
ctx.fillStyle = "rgb(40,40,240)";
ctx.fill();
ctx.closePath();
ctx.beginPath();
ctx.fillStyle = "rgb(0,0,0)";
ctx.arc(305,720,20,0,2*Math.PI,false);//1
ctx.arc(495,720,20,0,2*Math.PI,false);//2
ctx.fill();
ctx.closePath();
ctx.beginPath();//нос
ctx.lineWidth = 5;
ctx.moveTo(400,720);
ctx.lineTo(350,1000);
ctx.lineTo(400,1000);
ctx.stroke();
ctx.closePath();
ctx.beginPath();//зуб

ctx.closePath();
ctx.beginPath();//рот

ctx.closePath();