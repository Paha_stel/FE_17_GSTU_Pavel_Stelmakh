'use strict'
var inputTr = document.querySelector('table tr.input');
var submin = document.querySelector('[type="submit"]');
var text = document.querySelector('[type="text"]');

submin.addEventListener('click',AddElem);

function AddElem() {
    var tr = document.createElement('tr');
    var textTd = document.createElement('td');
    var date = new Date();
    textTd.innerText = date.toTimeString().match(/\d\d:\d\d:\d\d/) + ": " + text.value;
    //textTd.className = 'text';
    var butDel = document.createElement('input');
    butDel.type = 'submit';
    butDel.value = 'удалить';
    butDel.onclick = function () { Delete(this); }
    butDel.className = "delete";
    butDel.style.display = "none";
    butDel.style.width = "20px";
	butDel.style.backgroundColor = "white";
	butDel.style.borderColor = "white";
    var tdDel = document.createElement('td');
    tdDel.style.width = "100px";
    tdDel.className = "text";
	
    tdDel.onmouseenter = function () {
        butDel.style.display = "block";
        setTimeout(function () {
            butDel.style.width = "70px";
        },10);
    }
    tdDel.onmouseleave = function () {
        setTimeout(function () {
            butDel.style.width = "0px";
        },2000);
        setTimeout(function () {
            butDel.style.display = "none";
        },2800);
    }
	
    butDel.style.transition = "all 1s";
    tdDel.appendChild(butDel);
    tr.appendChild(textTd);
    tr.appendChild(tdDel);
    document.getElementsByTagName('tbody')[0].insertBefore(tr,inputTr);
}

function Delete(sub) {
    sub.parentNode.parentNode.parentNode.removeChild(sub.parentNode.parentNode);
}