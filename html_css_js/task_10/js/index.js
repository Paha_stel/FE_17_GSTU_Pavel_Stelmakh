'use strict'
var input = document.querySelector('input');
var divs = document.getElementsByClassName('field');
var i = 0, timer;

input.onclick = function () {
    timer = setInterval( function (){
        var req = new XMLHttpRequest();
        req.open('GET', 'http://localhost:3000/getItem?i=' + i, false);
        req.send();
        if (req.status == 200) {
            divs[i].innerHTML = req.responseText;
            i++;
        }
        if(i == 4){i = 0; clearInterval(timer);}
    },15);
}