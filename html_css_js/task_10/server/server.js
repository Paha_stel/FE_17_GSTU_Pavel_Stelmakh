var http = require('http');
var fs = require('fs');
var text = fs.readFileSync('../data/text.txt', 'utf8');

http.createServer(function (req,res) {
    if(req.url == '/'){
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/html');
        res.end(fs.readFileSync('../index.html', 'utf8'));
    }
    else if(req.url.match('.js')){
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/javascript');
        res.end(fs.readFileSync('..' + req.url, 'utf8'));
    }
    else if(req.url.match(/getItem\?i=\d/)){
        var reg = new RegExp('\r\n');
        var a = text.split(reg);
        setTimeout(function () {
            res.end(a[req.url.match(/\d/)]);
        },1000);
    }
    else{
        res.statusCode = 404;
        res.setHeader('Content-Type', 'text/plain');
        res.end('404: page not founded');
    }
}).listen(3000,'127.0.0.1',function () {
    console.log('server start listen');
});