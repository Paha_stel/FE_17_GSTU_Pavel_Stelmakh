﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;

namespace task_5_1
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = new StreamReader("index.html", Encoding.GetEncoding(1251));
            FileStream stream = new FileStream("indexNew.txt", FileMode.Create);
            StreamWriter writer = new StreamWriter(stream, Encoding.GetEncoding(1251));
            Regex regex = new Regex(@"(\<[^\>]+\>)|(\<\/[^\>]+\>)");
            string str;
            while (!reader.EndOfStream)
            {
                str = reader.ReadLine();
                MatchCollection match = regex.Matches(str);
                foreach (Match i in match) str = str.Replace(i.Value, "");
                writer.WriteLine(str);
            }
            reader.Close();
            writer.Close();
        }
    }
}
