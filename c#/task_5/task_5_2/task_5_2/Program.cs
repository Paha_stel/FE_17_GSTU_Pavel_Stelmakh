﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace task_5_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Directory directory = new Directory();
            directory.Reading();
            Bin(directory);
            Xml(directory);
            Json(directory);

            Console.ReadKey();
        }

        static void Bin(Directory directory)
        {
            Console.WriteLine("------------Бинарный--------------");
            BinaryFormatter formatter = new BinaryFormatter();
            using(FileStream stream = new FileStream("binary.dat", FileMode.OpenOrCreate))
            {
                formatter.Serialize(stream, directory);
            }
            using (FileStream stream = new FileStream("binary.dat", FileMode.OpenOrCreate))
            {
                Directory direcNew = (Directory)formatter.Deserialize(stream);
                direcNew.Out();
            }
        }

        static void Xml(Directory directory)
        {
            Console.WriteLine("------------XML--------------");
            XmlSerializer formatter = new XmlSerializer(typeof(Directory));
            using (FileStream stream = new FileStream("xml.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(stream, directory);
            }
            using (FileStream stream = new FileStream("xml.xml", FileMode.OpenOrCreate))
            {
                Directory direcNew = (Directory)formatter.Deserialize(stream);
                direcNew.Out();
            }
        }

        static void Json(Directory directory)
        {
            Console.WriteLine("------------JSON--------------");
            DataContractJsonSerializer formatter = new DataContractJsonSerializer(typeof(Directory));
            using (FileStream stream = new FileStream("json.json", FileMode.OpenOrCreate))
            {
                formatter.WriteObject(stream, directory);
            }
            using (FileStream stream = new FileStream("json.json", FileMode.OpenOrCreate))
            {
                Directory direcNew = (Directory)formatter.ReadObject(stream);
                direcNew.Out();
            }
        }

    }
}
