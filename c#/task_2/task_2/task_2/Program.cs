﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Test_1();
            Test_2();
            Test_3();
        }

        static void Test_1()
        {
            Console.Clear();
            Console.WriteLine("Test 1: int\n\n");
            Stack<int> stack = new Stack<int>(5);
            try
            {
                for (int i = 0; i < 6; i++)
                {
                    stack.Push(i + 1);
                    Console.WriteLine(stack.End());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
            while (!stack.Empty())
            {
                Console.WriteLine(stack.Pop());
            }

            try
            {
                stack.End();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }

        }

        static void Test_2()
        {
            Console.Clear();
            Console.WriteLine("Test 2: double\n\n");
            Stack<double> stack = new Stack<double>(4);
            try
            {
                for (double i = 0; i < 2.5; i+= 0.5)
                {
                    stack.Push(i);
                    Console.WriteLine(stack.End());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
            while (!stack.Empty())
            {
                Console.WriteLine(stack.Pop());
            }

            try
            {
                stack.End();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }

        }

        static void Test_3()
        {
            Console.Clear();
            Console.WriteLine("Test 3: string\n\n");
            Stack<string> stack = new Stack<string>(4);
            try
            {
                for (int i = 0; i < 5; i++)
                {
                    stack.Push("№ " + i);
                    Console.WriteLine(stack.End());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
            while (!stack.Empty())
            {
                Console.WriteLine(stack.Pop());
            }

            try
            {
                stack.End();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }

        }

    }
}
