﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryForTask_1
{
    public class Date: IComparable<Date>
    {
        int day;
        int month;
        int year;

        public Date(int day, int month, int year)
        {
            Day = day;
            Month = month;
            Year = year;
        }

        public Date()
            : this(15,10,2017) { }

        public int Day
        {
            get { return day; }
            set
            {
                if (value > 0 && value < 32) day = value;
                else throw new Exception("День не входит в доступный промежуток");
            }
        }

        public int Month
        {
            get { return month; }
            set
            {
                if (value > 0 && value < 13) month = value;
                else throw new Exception("Месяц не входит в доступный промежуток");
            }
        }

        public int Year
        {
            get { return year; }
            set
            {
                if (value > 2000 && value < 2020) year = value;
                else throw new Exception("Год не входит в доступный промежуток");
            }
        }

        public override string ToString()
        {
            return Day + ": " + Month + ": " + Year;
        }

        public int CompareTo(Date other)
        {
            if (Year - other.Year != 0) return Year.CompareTo(other.Year);
            if (Month - other.Month != 0) return Month.CompareTo(other.Month);
            return Day.CompareTo(other.Day);
        }
    }
}
