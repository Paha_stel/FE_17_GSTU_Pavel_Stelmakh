﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryForTask_1
{
    public class TechPassOfTheVeh : IComparable<TechPassOfTheVeh>
    {
        string registerSign;
        string mark;
        int yearOfIssue;
        double carMass;//в тоннах
        int engineCapacity;//в литрах
        Date registration;
        static int count = 0;

        public TechPassOfTheVeh(string regSig, string mark, int yIssue, double mCar,int enCap, Date reg)
        {
            RegisterSign = regSig;
            Mark = mark;
            YearOfIssue = yIssue;
            CarMass = mCar;
            EngineCapacity = enCap;
            Registration = reg;
            count++;
        }

        public string RegisterSign { 
            get { return registerSign; }
            private set { registerSign = value; } 
        }

        public string Mark
        {
            get { return mark; }
            private set { mark = value; }
        }

        public int YearOfIssue
        {
            get { return yearOfIssue; }
            private set { yearOfIssue = value; }
        }

        public double CarMass
        {
            get { return carMass; }
            private set { carMass = value; }
        }

        public int EngineCapacity
        {
            get { return engineCapacity; }
            private set { engineCapacity = value; }
        }

        public Date Registration
        {
            get { return registration; }
            private set { registration = value; }
        }

        public static string CountReg()
        {
            return "Количество зарегистрированных средств: " + count;
        }

        public override string ToString()
        {
            string s = Registration + " зарегестрирован: " + Mark + " с номером " + RegisterSign;
            s += "\nВыпущен: " + YearOfIssue + "\nМасса трю. средства: " + CarMass + "\nОбъем двигателя: " + EngineCapacity + "\n";
            return s;
        }

        public int CompareTo(TechPassOfTheVeh other)
        {
            return Registration.CompareTo(other.Registration);
        }
    }
}
