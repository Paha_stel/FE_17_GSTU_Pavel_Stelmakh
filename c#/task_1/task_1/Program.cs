﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryForTask_1;
using System.Globalization;

namespace task_1
{
    class Program
    {
        static IFormatProvider format = new NumberFormatInfo { NumberDecimalSeparator = "."};
        static void Main(string[] args)
        {
            Test();
        }

        static void Test()
        {
            TechPassOfTheVeh[] tachPass = new TechPassOfTheVeh[0];
            
            /*            
                        TechPassOfTheVeh[] tachPass = new TechPassOfTheVeh[] { new TechPassOfTheVeh("h012rus", "mazda", 2007, 1.5, 4, new Date()), new TechPassOfTheVeh("h010rus", "bmv", 2007, 2, 5, new Date(16,11,2010)),
                        new TechPassOfTheVeh("b111rus","audi",2008,1.5,5, new Date(31,7,2015)),new TechPassOfTheVeh("hb013rus","jiguly",2005,1,6,new Date(21,6,2016))};//запихать в файл
            */         
            int n = 0;
            do
            {
                Console.Clear();
                Console.WriteLine("Выберите пункт меню\n");
                Console.WriteLine("1. Зарегестрировать тр. средство");
                if (tachPass.Length != 0)
                {
                    Console.WriteLine("2. Вывести всю информацию о транспортных средствах");
                    Console.WriteLine("3. Вывод информации о количестве зарегистрировавшихся, после указанной даты");
                    Console.WriteLine("4. Вывод информации о тр. средстве по заданной марки");
                    Console.WriteLine("5. Вывод информации о тр. средстве по диапозону года выпуска");
                }
                Console.WriteLine("6. Выход");
                try
                {
                    n = Int32.Parse(Console.ReadLine());
                    if (tachPass.Length == 0 && n != 1) throw new Exception();
                }
                catch
                {
                    Console.WriteLine("Не верно ведены данные");
                }
                Console.Clear();
                switch (n)
                {
                    case 1: RegNew(ref tachPass);
                        break;
                    case 2:  foreach (TechPassOfTheVeh i in tachPass) Console.WriteLine(i); Console.ReadKey();
                        break;
                    case 3:
                        Console.Write("Введите дату регистрации (дд:мм:гг)(или пустую строку): ");
                        string date = Console.ReadLine();
                        Date reg;
                        if (date == "") reg = new Date();
                        else reg = new Date(Int32.Parse(date.Split(':')[0]), Int32.Parse(date.Split(':')[1]), Int32.Parse(date.Split(':')[2]));
                        OutRegAfterDate(tachPass, reg);
                        break;
                    case 4: Console.Write("Введите марку: ");
                        string marka = Console.ReadLine();
                        InfoByMark(tachPass, marka);
                        break;
                    case 5: Console.WriteLine("Введите диапозон дат");
                        int st = 0, end = 0;
                        try
                        {
                            st = Int32.Parse(Console.ReadLine());
                            end = Int32.Parse(Console.ReadLine());
                            if (end < st) throw new Exception();
                        }
                        catch
                        {
                            Console.WriteLine("Error");
                            Console.ReadKey();
                        }
                        InfoInPeriod(tachPass,st,end);

                        break;
                    case 6: n = 0;
                        break;
                    default: Console.WriteLine("Такого номера нет"); break;
                }
            } while (n != 0);

            Console.ReadKey();
        }

        static void RegNew(ref TechPassOfTheVeh[] tachPass)
        {
            Array.Resize(ref tachPass, tachPass.Length + 1);
            bool good = false;
            do
            {
                try
                {
                    Console.Write("Введите регистрационный номер: ");
                    string regSig = Console.ReadLine();
                    Console.Write("Введите марку: ");
                    string mark = Console.ReadLine();
                    Console.Write("Введите год выпуска: ");
                    int yIssue = Int32.Parse(Console.ReadLine()); ;
                    Console.Write("Введите массу машины (в тоннах): ");
                    double mCar = Double.Parse(Console.ReadLine(), format); ;
                    Console.Write("Введите объем двигателя (в литрах): ");
                    int enCap = Int32.Parse(Console.ReadLine()); ;
                    Console.Write("Введите дату регистрации (дд:мм:гг)(или пустую строку): ");
                    string date = Console.ReadLine();
                    Date reg;
                    if (date == "") reg = new Date();
                    else reg = new Date(Int32.Parse(date.Split(':')[0]), Int32.Parse(date.Split(':')[1]), Int32.Parse(date.Split(':')[2]));
                    tachPass[tachPass.Length - 1] = new TechPassOfTheVeh(regSig, mark, yIssue, mCar, enCap, reg);
                    good = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadKey();
                }
            } while (!good);
        }

        static void OutRegAfterDate(TechPassOfTheVeh[] tachPass, Date date)
        {
            int count = 0;
            for (int i = 0; i < tachPass.Length; i++)
            {
                if (date.CompareTo(tachPass[i].Registration) == -1) count++;
            }
            Console.WriteLine("Кол-во зарегистрированных: " + count);
            Console.ReadKey();
        }

        static void InfoByMark(TechPassOfTheVeh[] tachPass,string mark)
        {
            for (int i = 0; i < tachPass.Length; i++) 
            {
                if (tachPass[i].Mark == mark)
                {
                    Console.WriteLine(tachPass[i]);
                    Console.ReadKey();
                    return;
                }
            }
            Console.WriteLine("Транспортоное средство не найденно");
            Console.ReadKey();
        }

        static void InfoInPeriod(TechPassOfTheVeh[] tachPass, int start, int end)
        {
            bool have = false;
            for (int i = 0; i < tachPass.Length; i++)
            {
                if (tachPass[i].YearOfIssue >= start && tachPass[i].YearOfIssue <= end)
                {
                    Console.WriteLine(tachPass[i]);
                    have = true;
                }
            }
            if (!have) Console.WriteLine("Транспортоное средство не найденно");
            Console.ReadKey();
        }
    }
}
