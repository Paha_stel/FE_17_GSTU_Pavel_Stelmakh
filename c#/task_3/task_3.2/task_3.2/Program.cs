﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task_2;
using StackChar = task_2.Stack<char>;
using StackInt = task_2.Stack<int>;

namespace task_3._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Mathem();
        }

        static void Mathem()
        {
            string str;
            do {
                Console.Clear();
                Console.WriteLine("Введите выражение(без пробелов):");
                str = Console.ReadLine();
            }while(!Check(str));
            string strOPZ = TranslateInOPZ(str);
            Console.WriteLine(str + " = " + Computation(strOPZ));
            Console.ReadKey();

        }

        static bool Check(string str)
        {
            int countSc = 0, countPl = 0, countNeg = 0, countMul = 0, countDiv = 0;
            try
            {
                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i] == ' ') throw new Exception("Пробелы запрещены");
                    if (str[i] == '+') countPl++;
                    if (str[i] == '-') countNeg++;
                    if (str[i] == '*') countMul++;
                    if (str[i] == '/') countDiv++;
                    if (str[i] == '(') countSc++;
                    if (str[i] == ')') countSc--;
                    if(countSc < 0) throw new Exception("Неверно расставлены скобки");
                }
                if (countSc != 0) throw new Exception("Неверно расставлены скобки");
                Regex regPl = new Regex(@"(\d|\))\+(\d|\()(\+(\d|\())*");
                Regex regNeg = new Regex(@"(\d|\))\-(\d|\()(\-(\d|\())*");
                Regex regMul = new Regex(@"(\d|\))\*(\d|\()(\*(\d|\())*");
                Regex regDiv = new Regex(@"(\d|\))\/(\d|\()(\/(\d|\())*");
                Regex regSc = new Regex(@"(\d\()|(\)\d)");
                int n = 0;
                MatchCollection a = regPl.Matches(str);
                foreach (Match i in a)
                {
                    string chr = i.Value;
                    n += (new Regex(@"\+")).Matches(chr).Count;

                }
                if (countPl != n) throw new Exception("Неверно расставлены +");
                n = 0;
                a = regNeg.Matches(str);
                foreach (Match i in a)
                {
                    string chr = i.Value;
                    n += (new Regex(@"\-")).Matches(chr).Count;

                }
                if (countNeg != n) throw new Exception("Неверно расставлены -");
                n = 0;
                a = regMul.Matches(str);
                foreach (Match i in a)
                {
                    string chr = i.Value;
                    n += (new Regex(@"\*")).Matches(chr).Count;

                }
                if (countMul != n) throw new Exception("Неверно расставлены *");
                n = 0;
                a = regDiv.Matches(str);
                foreach (Match i in a)
                {
                    string chr = i.Value;
                    n += (new Regex(@"\/")).Matches(chr).Count;

                }
                if (countDiv != n) throw new Exception("Неверно расставлены /");
                if(regSc.Matches(str).Count != 0) throw new Exception("Неверно расставлены ( )");

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                return false;
            }
        }

        static string TranslateInOPZ(string str)
        {
            StackChar stackChar = new StackChar(str.Length / 2);
            string strOPZ = "";
            char[] prior1 = { '+', '-' };
            char[] prior2 = { '*', '/' };
            for (int i = 0; i < str.Length; i++)
            {
                if (Char.IsDigit(str[i])) {
                    if (i + 1 != str.Length && Char.IsDigit(str[i + 1])) strOPZ += str[i];
                    else strOPZ += str[i] + " ";
                }
                else
                {
                    if (stackChar.Empty() || str[i] == '(') stackChar.Push(str[i]);
                    else
                    {
                        if (str[i] == ')')
                        {
                            char k = stackChar.Pop();
                            while (k != '(')
                            {
                                strOPZ += k;
                                k = stackChar.Pop();
                            }
                        }
                        else
                        {
                            if (((stackChar.End() == prior2[0] || stackChar.End() == prior2[1])
                                && (str[i] == prior1[0] || str[i] == prior1[1]))
                                || ((stackChar.End() == prior1[0] || stackChar.End() == prior1[1])
                                && (str[i] == prior1[0] || str[i] == prior1[1]))
                                || ((stackChar.End() == prior2[0] || stackChar.End() == prior2[1])
                                && (str[i] == prior2[0] || str[i] == prior2[1])))
                            {
                                do
                                {
                                    strOPZ += stackChar.Pop(); ;
                                    if (stackChar.Empty()) break;
                                } while (((stackChar.End() == prior2[0] || stackChar.End() == prior2[1])
                                && (str[i] == prior1[0] || str[i] == prior1[1]))
                                || ((stackChar.End() == prior1[0] || stackChar.End() == prior1[1])
                                && (str[i] == prior1[0] || str[i] == prior1[1]))
                                || ((stackChar.End() == prior2[0] || stackChar.End() == prior2[1])
                                && (str[i] == prior2[0] || str[i] == prior2[1])));
                                stackChar.Push(str[i]);
                            }
                            else stackChar.Push(str[i]);
                        }
                    }
                }
            }
            while (!stackChar.Empty()) strOPZ += stackChar.Pop();


            return strOPZ;
        }

        static int Computation(string str)
        {
            string digit = "";
            StackInt stackInt = new StackInt(str.Length);
            for(int i = 0; i < str.Length; i++)
            {
                if (Char.IsDigit(str[i]))
                {
                    digit += str[i];
                    if (i + 1 != str.Length && Char.IsDigit(str[i + 1])) continue;
                    stackInt.Push(Int32.Parse(digit));
                    digit = "";
                }
                else
                {
                    if (str[i] != ' ')
                    {
                        int opr2 = stackInt.Pop();
                        int opr1 = stackInt.Pop();
                        if (str[i] == '+') stackInt.Push(opr1 + opr2);
                        if (str[i] == '-') stackInt.Push(opr1 - opr2);
                        if (str[i] == '*') stackInt.Push(opr1 * opr2);
                        if (str[i] == '/') stackInt.Push(opr1 / opr2);
                    }
                }
            }


            return stackInt.Pop(); 
        }

    }
}
