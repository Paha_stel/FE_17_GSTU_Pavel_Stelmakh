﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_2
{
    public class Stack<T>
    {
        T[] data = new T[0];
        int size;

        public Stack(int MaxSize)
        {
            size = MaxSize;
        }

        public void Push(T val)
        {
            if (Filled()) throw new Exception("stack is filled");
            Array.Resize<T>(ref data, data.Length + 1);
            data[data.Length - 1] = val;
        }

        public T Pop()
        {
            if (Empty()) throw new Exception("stack is empty");
            T temp = data[data.Length - 1];
            Array.Resize<T>(ref data, data.Length - 1);
            return temp;
        }

        public bool Filled()
        {
            return data.Length == size;
        }

        public bool Empty()
        {
            return data.Length == 0;
        }

        public T End()
        {
            if (Empty()) throw new Exception("stack is empty");
            return data[data.Length - 1];
        }
    }
}
