﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true) {
                Console.WriteLine("\n\nВведите строку:");
                StringInfo.Input();
                Console.Clear();
                Console.WriteLine("Строка: " + StringInfo.Str);
                Console.WriteLine("Общее кол-во символов: " + StringInfo.Count);
                Console.WriteLine("Кол-во букв в нижнем регистре: " + StringInfo.CountLowLetter);
                Console.WriteLine("Кол-во букв в верхнем регистре: " + StringInfo.CountUpLetter);
                Console.WriteLine("Кол-во символов пунктуации: " + StringInfo.CountPunctuationSimbol);
                Console.WriteLine("Кол-во пробельных символов: " + StringInfo.CountSpaceSimbol);
            }
        }
    }
}
