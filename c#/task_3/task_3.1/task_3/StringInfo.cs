﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_3
{
    abstract class StringInfo
    {
        static string str = "";
        public static string Str { get { return str; } }

        public static int Count { get { return str.Length; } }

        public static void Input()
        {
            str = Console.ReadLine();
        }

        public static int CountLowLetter { get {
                int count = 0;
                for(int i = 0; i < str.Length; i++)
                {
                    if (Char.IsLower(str[i])) count++;
                }
                return count;
            } }

        public static int CountUpLetter
        {
            get
            {
                int count = 0;
                for (int i = 0; i < str.Length; i++)
                {
                    if (Char.IsUpper(str[i])) count++;
                }
                return count;
            }
        }

        public static int CountSpaceSimbol
        {
            get
            {
                int count = 0;
                for (int i = 0; i < str.Length; i++)
                {
                    if (Char.IsWhiteSpace(str[i])) count++;
                }
                return count;
            }
        }

        public static int CountPunctuationSimbol
        {
            get
            {
                int count = 0;
                for (int i = 0; i < str.Length; i++)
                {
                    if (Char.IsPunctuation(str[i])) count++;
                }
                return count;
            }
        }

    }
}
