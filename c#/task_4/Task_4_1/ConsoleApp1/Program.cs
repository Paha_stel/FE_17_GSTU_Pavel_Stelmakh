﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите текст: ");
            string text = Console.ReadLine();
            string[] words = Words(text);
            var freW = FrequencyOfWord(words);
            for (int i = 0; i < freW.Item1.Length; i++) Console.WriteLine(freW.Item2[i] + " - " + freW.Item1[i]);

            Console.ReadKey();
        }

        static string[] Words(string text)
        {
            Regex regex = new Regex(@"\w+");
            MatchCollection regWords = regex.Matches(text);
            string[] words = new string[regWords.Count];
            int j = 0;
            foreach(Match i in regWords)
            {
                words[j] = i.Value;
                j++;
            }
            return words;
        }

        static (int[], string[]) FrequencyOfWord(string[] words)
        {
            string[] uniWrods = new string[0];

            for(int i = 0, j = 0; i < words.Length; i++)
            {
                if (uniWrods.Length == 0)
                {
                    Array.Resize(ref uniWrods, uniWrods.Length + 1);
                    uniWrods[j] = words[i]; j++;
                }
                else
                {
                    bool have = false;
                    foreach(string k in uniWrods) if (k == words[i]) have = true;
                    if (!have)
                    {
                        Array.Resize(ref uniWrods, uniWrods.Length + 1);
                        uniWrods[j] = words[i]; j++;
                    }
                }
            }

            int[] freW = new int[uniWrods.Length];
            for (int i = 0; i < uniWrods.Length; i++)
            {
                foreach (string j in words)
                {
                    if (uniWrods[i] == j) freW[i]++;
                }
            }

            return (freW, uniWrods);
        }
    }
}
