﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Task_4_2
{
    class Firm
    {
        string name;
        string owner;
        string phone;
        string address;
        string field;

        public Firm()
        {
            Input();
        }

        public Firm(string n, string own, string ph, string addrss, string f)
        {
            Name = n;
            Owner = own;
            Phone = ph;
            Address = addrss;
            Field = f;
        }

        void Input()
        {
            bool good = false;
            Regex regex = new Regex(@"\d\d-\d\d-\d\d");
            do
            {
                try
                {
                    Console.Clear();
                    Console.WriteLine("Введите название фирмы");
                    name = Console.ReadLine();
                    if (name.Length < 2) throw new Exception("Недопустимое название");
                    Console.WriteLine("Введите имя владельца");
                    owner = Console.ReadLine();
                    if (owner.Length < 2) throw new Exception("Недопустимое имя");
                    Console.WriteLine("Введите телефон (11-11-11)");
                    phone = Console.ReadLine();
                    if (!regex.IsMatch(phone)) throw new Exception("Неверная форма записи телефона");
                    Console.WriteLine("Введите адрес");
                    address = Console.ReadLine();
                    if (address.Length < 2) throw new Exception("Недопустимый адресс");
                    Console.WriteLine("Введите род деятельности");
                    field = Console.ReadLine();
                    if (field.Length < 2) throw new Exception("Недопустимое поле деятельность");

                    good = true;
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadKey();
                }
            } while (!good);
        }

        public string Name {
            get { return name; }
            set
            {
                if (value.Length < 2) throw new Exception("Недопустимое название");
                name = value;
            }
        }
        public string Owner
        {
            get { return owner; }
            set
            {
                if (value.Length < 2) throw new Exception("Недопустимое имя");
                owner = value;
            }
        }
        public string Phone
        {
            get { return phone; }
            set
            {
                Regex regex = new Regex(@"\d\d-\d\d-\d\d");
                if (!regex.IsMatch(value)) throw new Exception("Неверная форма записи телефона");
                phone = value;
            }
        }
        public string Address
        {
            get { return address; }
            set
            {
                if (value.Length < 2) throw new Exception("Недопустимый адресс");
                address = value;
            }
        }
        public string Field
        {
            get { return field; }
            set
            {
                if (value.Length < 2) throw new Exception("Недопустимое поле деятельность");
                field = value;
            }
        }

    }
}
