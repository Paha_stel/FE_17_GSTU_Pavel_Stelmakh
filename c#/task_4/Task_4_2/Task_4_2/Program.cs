﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;

namespace Task_4_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Firm[] firms = new Firm[10];
            StreamReader reader = new StreamReader("text.txt",Encoding.GetEncoding(1251));
            string n, ow, tl, ad, f;
            for(int i = 0; i < 10; i++)
            {
                n = reader.ReadLine();
                ow = reader.ReadLine();
                tl = reader.ReadLine();
                ad = reader.ReadLine();
                f = reader.ReadLine();
                firms[i] = new Firm(n, ow, tl, ad, f);
            }
            Out(firms);
            FName(firms);
            FOwner(firms);
            FOwnerRange(firms);
            FPhone(firms);
            FField(firms);

            Console.ReadKey();
        }

        static void Out(Firm[] firms)
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(firms[i].Name + " -> " + firms[i].Owner + " " + firms[i].Phone);
                Console.WriteLine(firms[i].Field + "\n" + firms[i].Address + "\n\n\n");
            }
        }

        static void FName(Firm[] firms)
        {
            Console.WriteLine("------------------------Поиск по названию----------------");
            string name = "Шпа";
            //Console.WriteLine("Введите название");
            //name = Console.ReadLine();
            var firmN = firms.Where(i => i.Name == name);
            foreach(var i in firmN)
            {
                Console.WriteLine(i.Name + " -> " + i.Owner + " " + i.Phone);
                Console.WriteLine(i.Field + "\n" + i.Address + "\n\n\n");
            }
        }

        static void FOwner(Firm[] firms)
        {
            Console.WriteLine("------------------------Поиск по владельцу----------------");
            string owner = "Вася";
            //Console.WriteLine("Введите имя");
            //owner = Console.ReadLine();
            var firmN = firms.Where(i => i.Owner == owner);
            foreach (var i in firmN)
            {
                Console.WriteLine(i.Name + " -> " + i.Owner + " " + i.Phone);
                Console.WriteLine(i.Field + "\n" + i.Address + "\n\n\n");
            }
        }

        static void FOwnerRange(Firm[] firms)
        {
            Console.WriteLine("------------------------Поиск по диапазону фамилий----------------");
            string l = "П",r = "Т";
            //Console.WriteLine("Введите левый и правый пределы");
            //l = Console.ReadLine();
            //r = Console.ReadLine();
            //if (r.Length != 1 && l.Length != 1) throw new Exception("Неверный ввод диапазона");
            var firmN = firms.Where(i => i.Owner[0] >= l.ToCharArray()[0] && i.Owner[0] <= r.ToCharArray()[0]);
            foreach (var i in firmN)
            {
                Console.WriteLine(i.Name + " -> " + i.Owner + " " + i.Phone);
                Console.WriteLine(i.Field + "\n" + i.Address + "\n\n\n");
            }
        }

        static void FPhone(Firm[] firms)
        {
            Console.WriteLine("------------------------Поиск по телефону----------------");
            string phone = "41-33-22";
            //Console.WriteLine("Введите телефон");
            //phone = Console.ReadLine();
            //Regex regex = new Regex(@"\d\d-\d\d-\d\d");
            //if (!regex.IsMatch(phone)) throw new Exception("Неверная форма записи телефона");
            var firmN = firms.Where(i => i.Phone == phone);
            foreach (var i in firmN)
            {
                Console.WriteLine(i.Name + " -> " + i.Owner + " " + i.Phone);
                Console.WriteLine(i.Field + "\n" + i.Address + "\n\n\n");
            }
        }

        static void FField(Firm[] firms)
        {
            Console.WriteLine("------------------------Поиск по роду деятельности----------------");
            string field = "шпоры";
            //Console.WriteLine("Введите род деятельности");
            //field = Console.ReadLine();
            var firmN = firms.Where(i => i.Field == field);
            foreach (var i in firmN)
            {
                Console.WriteLine(i.Name + " -> " + i.Owner + " " + i.Phone);
                Console.WriteLine(i.Field + "\n" + i.Address + "\n\n\n");
            }
        }

    }
}
